function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DonutChart =
/*#__PURE__*/
function () {
  function DonutChart() {
    _classCallCheck(this, DonutChart);

    this.options = {
      svgClassName: null,
      partGap: 5,
      donutWidth: 25,
      donutRadius: 100,
      animationTimeInMs: 800,
      label: {
        offset: 65,
        fontSize: 15,
        name: "Not set"
      },
      colors: {
        donutPart: "#e9e9e9",
        prognosisBar: "#9EBCA8",
        progressBar: "#5C8D6D"
      },
      legend: {
        progress: {
          position: {
            y: 180,
            x: 173
          },
          name: "Not set"
        },
        prognosis: {
          position: {
            y: 225,
            x: 173
          },
          name: "Not set"
        }
      }
    };
    this.namespaceURI = "http://www.w3.org/2000/svg";
  }
  /**
   * Initializes donut part if data is set.
   */


  _createClass(DonutChart, [{
    key: "init",
    value: function init() {
      var _this = this;

      // Generates styles element with keyframes for the donut chart animation and appends it to the header
      this.setDonutChartStyles(); // Create svg element.

      this.svgEl = this.generateSvgEl(); // Render donut parts and labels

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.donutData.listOfDonutParts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var part = _step.value;
          this.renderDonutPart(part, "part");
          this.renderDonutPart(part, "prognosisPart");
          this.renderDonutPart(part, "progressPart");
          this.renderDonutPartLabel(part);
        } // Render gap between the parts

      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = this.donutData.listOfDonutParts[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var _part = _step2.value;
          this.renderDonutPartGap(_part);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      this.renderDonutChartLegend(); // Adjust font-size of the text if svg is resized.

      window.addEventListener("resize", function () {
        return _this.adjustFontSize();
      });
    }
  }, {
    key: "renderDonutChartLegend",
    value: function renderDonutChartLegend() {
      var donutChartLegend = document.createElementNS(this.namespaceURI, "g");
      donutChartLegend.setAttribute("class", "donut-chart__legend");
      var progressLegendCircle = document.createElementNS(this.namespaceURI, "circle");
      progressLegendCircle.setAttribute("cy", this.options.legend.progress.position.y);
      progressLegendCircle.setAttribute("cx", "160");
      progressLegendCircle.setAttribute("r", "7");
      progressLegendCircle.setAttribute("fill", this.options.colors.progressBar);
      var prognosisLegendCircle = document.createElementNS(this.namespaceURI, "circle");
      prognosisLegendCircle.setAttribute("cy", this.options.legend.prognosis.position.y);
      prognosisLegendCircle.setAttribute("cx", "160");
      prognosisLegendCircle.setAttribute("r", "7");
      prognosisLegendCircle.setAttribute("fill", this.options.colors.prognosisBar);
      donutChartLegend.appendChild(this.generateTextEl(this.options.legend.progress.name, [{
        name: "y",
        value: this.options.legend.progress.position.y
      }, {
        name: "x",
        value: this.options.legend.progress.position.x
      }, {
        name: "class",
        value: "label"
      }]));
      donutChartLegend.appendChild(this.generateTextEl("".concat(this.donutData.progressVolume, "hl"), [{
        name: "y",
        value: this.options.legend.progress.position.y
      }, {
        name: "x",
        value: this.options.legend.progress.position.x
      }, {
        name: "class",
        value: "volume"
      }]));
      donutChartLegend.appendChild(this.generateTextEl(this.options.legend.prognosis.name, [{
        name: "y",
        value: this.options.legend.prognosis.position.y
      }, {
        name: "x",
        value: this.options.legend.prognosis.position.x
      }, {
        name: "class",
        value: "label"
      }]));
      donutChartLegend.appendChild(this.generateTextEl("".concat(this.donutData.prognosisVolume, "hl"), [{
        name: "y",
        value: this.options.legend.prognosis.position.y
      }, {
        name: "x",
        value: this.options.legend.prognosis.position.x
      }, {
        name: "class",
        value: "volume"
      }]));
      donutChartLegend.appendChild(prognosisLegendCircle);
      donutChartLegend.appendChild(progressLegendCircle);
      this.svgEl.appendChild(donutChartLegend);
    }
    /**
     * Generates styles element with keyframes for the donut chart animation and appends it to the header.
     */

  }, {
    key: "setDonutChartStyles",
    value: function setDonutChartStyles() {
      var css = ""; // Add keyframes for animation

      css += "@keyframes donut-chart {from {stroke-dasharray: 0, ".concat(this.getCircleOutlineLength(this.options.donutRadius), ";}}");
      css += "svg .label {font-weight: bold}";
      css += ".upper-label-text {font-weight: bold}";
      css += "@media only screen and (max-width: 330px) {svg .donut-chart__legend {display: none;}}"; // If css is present append it the head

      if (css) {
        var stylesEl = document.createElement("style");
        stylesEl.setAttribute("type", "text/css");
        stylesEl.innerHTML = css;
        document.head.appendChild(stylesEl);
      }
    }
    /**
     * Render part of the donut
     * @param part {Object} Starting point of donut part in percentage
     * @param type  {string} Length of donut part in percentage
     */

  }, {
    key: "renderDonutPart",
    value: function renderDonutPart(part, type) {
      var partLengthInDegrees, color, animation, dashLength, startingPoint;
      var circleOutlineLength = this.getCircleOutlineLength(this.options.donutRadius);

      switch (type) {
        case "part":
          partLengthInDegrees = part.length.valueInDegrees;
          color = this.options.colors.donutPart;
          break;

        case "progressPart":
          partLengthInDegrees = part.progress.valueInDegrees;
          color = this.options.colors.progressBar;
          animation = {
            time: part.progress.animationTime,
            delay: part.progress.animationDelayTime,
            timingFunction: "linear"
          };

          if (part.progress.valueInPercent < 100 && part.progress.valueInPercent !== 0) {
            if (part.prognosis.valueInPercent !== 100 && part.prognosis.valueInPercent !== 0) {
              if (part.prognosis.valueInPercent < part.progress.valueInPercent) {
                animation.time += part.prognosis.animationTime;
                animation.timingFunction = "ease-out";
              }
            } else {
              if (part.prognosis.valueInPercent < part.progress.valueInPercent) {
                animation.timingFunction = "ease-out";
              }
            }
          }

          break;

        case "prognosisPart":
          partLengthInDegrees = part.prognosis.valueInDegrees;
          color = this.options.colors.prognosisBar;
          animation = {
            time: part.prognosis.animationTime,
            delay: part.prognosis.animationDelayTime,
            timingFunction: "linear"
          };

          if (part.prognosis.valueInPercent < 100 && part.prognosis.valueInPercent !== 0) {
            if (part.progress.valueInPercent !== 100 && part.progress.valueInPercent !== 0) {
              if (part.progress.valueInPercent < part.prognosis.valueInPercent) {
                animation.time += part.prognosis.animationTime;
                animation.timingFunction = "ease-out";
              }
            } else {
              if (part.progress.valueInPercent < part.prognosis.valueInPercent) {
                animation.timingFunction = "ease-out";
              }
            }
          }

          break;

        default:
          console.error("Type has not been set");
          break;
      } // Add gap to the donut


      partLengthInDegrees -= this.options.partGap; // Prevent negative partLength value

      if (partLengthInDegrees < 0) {
        partLengthInDegrees = 0;
      } // Correct starting position of all circles.


      startingPoint = part.min.valueInDegrees - 90; // Correct rotation of the circle if gap is set

      startingPoint += this.options.partGap / 2; // Calculate outline of circle for rendering

      dashLength = circleOutlineLength / 360 * partLengthInDegrees;
      var circleEl = this.generateCircleEl([{
        name: "stroke-dasharray",
        value: "".concat(dashLength, ", ").concat(circleOutlineLength)
      }, {
        name: "transform",
        value: "rotate(".concat(startingPoint, ", 200, 200)")
      }, {
        name: "stroke",
        value: color
      }], animation);
      this.svgEl.appendChild(circleEl);
    }
    /**
     * Generates gap between the donut part
     *
     * @param part {Object}
     * @returns {SVGCircleElement}
     */

  }, {
    key: "renderDonutPartGap",
    value: function renderDonutPartGap(part) {
      var startingPoint = part.min.valueInDegrees; // Correct starting position of all circles.

      startingPoint -= 90; // Calculate degrees, radians and radius

      var degrees = part.min.valueInDegrees;
      var radians = this.convertDegreesToRadians(degrees);
      var radius = this.options.donutRadius - 15; // Calculate coordinates of the label

      var x = 200 + radius * Math.sin(radians);
      var y = 200 - radius * Math.cos(radians);
      var rect = this.generateRectangleEl([{
        name: "x",
        value: x - 1
      }, {
        name: "y",
        value: y - this.options.partGap
      }, {
        name: "transform",
        value: "rotate(".concat(startingPoint, " ").concat(x, " ").concat(y, ")")
      }]);
      this.svgEl.appendChild(rect);
    }
    /**
     * Generates circles that looks like parts
     *
     * @param attributes
     * @param animation
     * @returns {SVGCircleElement}
     */

  }, {
    key: "generateCircleEl",
    value: function generateCircleEl(attributes, animation) {
      var circle = document.createElementNS(this.namespaceURI, "circle");
      circle.setAttribute("cy", "200");
      circle.setAttribute("cx", "200");
      circle.setAttribute("stroke-width", this.options.donutWidth.toString());
      circle.setAttribute("r", this.options.donutRadius.toString());
      circle.setAttribute("fill", "none");
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = attributes[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var attribute = _step3.value;
          circle.setAttribute(attribute.name, attribute.value);
        }
      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
            _iterator3["return"]();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }

      if (animation) {
        circle.style.animationTimingFunction = animation.timingFunction;
        circle.style.animationDelay = "".concat(animation.delay, "ms");
        circle.style.animationDuration = "".concat(animation.time, "ms");
        circle.style.animationName = "donut-chart";
        circle.style.animationFillMode = "backwards";
      }

      return circle;
    }
    /**
     *
     * Generates gap between donut parts
     * @param attributes {object} Part data from donut data
     * @returns {SVGRectElement}
     */

  }, {
    key: "generateRectangleEl",
    value: function generateRectangleEl(attributes) {
      var rect = document.createElementNS(this.namespaceURI, "rect");
      rect.setAttribute("width", "30");
      rect.setAttribute("height", (this.options.partGap * 2).toString());
      rect.setAttribute("fill", "white");
      var _iteratorNormalCompletion4 = true;
      var _didIteratorError4 = false;
      var _iteratorError4 = undefined;

      try {
        for (var _iterator4 = attributes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
          var attribute = _step4.value;
          rect.setAttribute(attribute.name, attribute.value);
        }
      } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion4 && _iterator4["return"] != null) {
            _iterator4["return"]();
          }
        } finally {
          if (_didIteratorError4) {
            throw _iteratorError4;
          }
        }
      }

      return rect;
    }
    /**
     *
     * Generates label next to donut part
     * @param part {object} Part data from donut data
     */

  }, {
    key: "renderDonutPartLabel",
    value: function renderDonutPartLabel(part) {
      // Calculate degrees, radians and radius
      var degrees = part.length.valueInDegrees / 2 + part.min.valueInDegrees;
      var radians = this.convertDegreesToRadians(degrees);
      var radius = this.options.donutRadius + this.options.label.offset; // Calculate coordinates of the label

      var x = 200 + radius * Math.sin(radians);
      var y = 200 - radius * Math.cos(radians); // Generate labels

      var firstLabelText = "".concat(this.options.label.name, " ").concat(part.order); // Last part should not show max value

      var secondLabelText;

      switch (part.order) {
        case 1:
          secondLabelText = "".concat(part.min.value, " - ").concat(part.max.value, "hl");
          break;

        case this.donutData.totalParts:
          secondLabelText = "".concat(part.min.value + 1, "hl +");
          break;

        default:
          secondLabelText = "".concat(part.min.value + 1, " - ").concat(part.max.value, "hl");
      } // Render the label


      this.svgEl.appendChild(this.generateTextEl(firstLabelText, [{
        name: "x",
        value: x
      }, {
        name: "y",
        value: y
      }, {
        name: "text-anchor",
        value: "middle"
      }, {
        name: "class",
        value: "upper-label-text"
      }]));
      this.svgEl.appendChild(this.generateTextEl(secondLabelText, [{
        name: "x",
        value: x
      }, {
        name: "y",
        value: y
      }, {
        name: "text-anchor",
        value: "middle"
      }, {
        name: "class",
        value: "lower-label-text"
      }]));
    }
    /**
     * Adjust font-size of the text in donut chart if user is resizing the svg image.
     */

  }, {
    key: "adjustFontSize",
    value: function adjustFontSize() {
      var newWidth = this.svgEl.getBoundingClientRect().width;
      var newFontSize = this.options.label.fontSize * (400 / newWidth);
      var textEls = this.svgEl.querySelectorAll("text");
      var _iteratorNormalCompletion5 = true;
      var _didIteratorError5 = false;
      var _iteratorError5 = undefined;

      try {
        for (var _iterator5 = textEls[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
          var textEl = _step5.value;
          textEl.setAttribute("font-size", newFontSize.toString());

          if (textEl.classList.contains("lower-label-text") || textEl.classList.contains("volume")) {
            textEl.setAttribute("dy", (newFontSize * 1.2).toString());
          }
        }
      } catch (err) {
        _didIteratorError5 = true;
        _iteratorError5 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion5 && _iterator5["return"] != null) {
            _iterator5["return"]();
          }
        } finally {
          if (_didIteratorError5) {
            throw _iteratorError5;
          }
        }
      }
    }
    /**
     *
     * Generates svg tspan element
     * @param text {string}
     * @param attributes {object[]} List of attributes
     * @returns {SVGTextElement}
     */

  }, {
    key: "generateTextEl",
    value: function generateTextEl(text, attributes) {
      var textEl = document.createElementNS(this.namespaceURI, "text");
      var _iteratorNormalCompletion6 = true;
      var _didIteratorError6 = false;
      var _iteratorError6 = undefined;

      try {
        for (var _iterator6 = attributes[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
          var attribute = _step6.value;
          textEl.setAttribute(attribute.name, attribute.value);
        }
      } catch (err) {
        _didIteratorError6 = true;
        _iteratorError6 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion6 && _iterator6["return"] != null) {
            _iterator6["return"]();
          }
        } finally {
          if (_didIteratorError6) {
            throw _iteratorError6;
          }
        }
      }

      textEl.innerHTML = text;
      return textEl;
    }
    /**
     * Creates svg element where donut part will be rendered.
     */

  }, {
    key: "generateSvgEl",
    value: function generateSvgEl() {
      var svgEl = document.createElementNS(this.namespaceURI, "svg");
      svgEl.setAttribute("width", "100%");
      svgEl.setAttribute("height", "100%");
      svgEl.setAttribute("viewBox", "0 0 400 400");
      if (this.options.svgClassName) svgEl.classList.add(this.options.svgClassName);
      return svgEl;
    }
    /**
     * Converts degrees to radians
     * @param degrees {number} Degrees
     */

  }, {
    key: "convertDegreesToRadians",
    value: function convertDegreesToRadians(degrees) {
      return degrees / (180 / Math.PI);
    }
    /**
     * Enrich data for the donut chart
     * @param listOfDonutParts {array} List of donut parts
     */

  }, {
    key: "processData",
    value: function processData(listOfDonutParts) {
      // Create data structure, set total parts and total length of the parts
      var donutData = {
        listOfDonutParts: [],
        totalParts: listOfDonutParts.length,
        totalLengthOfParts: listOfDonutParts[listOfDonutParts.length - 1].max
      }; // Variables are for calculating animation delay

      var sumProgressAnimationDelayTime = 0;
      var sumPrognosisAnimationDelayTime = 0;

      for (var i = 0; i < listOfDonutParts.length; i++) {
        var donutPart = listOfDonutParts[i];
        var enrichedDonutPart = {};

        if (i !== 0) {
          donutPart.min -= 1;
        }

        enrichedDonutPart.order = donutPart.index + 1;
        enrichedDonutPart.min = {
          value: donutPart.min,
          valueInDegrees: 360 / donutData.totalParts * i
        };
        enrichedDonutPart.max = {
          value: donutPart.max
        };
        enrichedDonutPart.length = {
          value: donutData.totalLengthOfParts / donutData.totalParts,
          valueInDegrees: 360 / donutData.totalParts
        };
        enrichedDonutPart.prognosis = {
          valueInPercent: donutPart.prognosis,
          valueInDegrees: enrichedDonutPart.length.valueInDegrees / 100 * donutPart.prognosis,
          animationDelayTime: null,
          animationTime: null
        };
        enrichedDonutPart.progress = {
          valueInPercent: donutPart.progress,
          valueInDegrees: enrichedDonutPart.length.valueInDegrees / 100 * donutPart.progress,
          animationDelayTime: null,
          animationTime: null
        }; // Store maximum animation time per part for later use

        var maximumAnimationTimePerPart = this.options.animationTimeInMs / 360 * enrichedDonutPart.length.valueInDegrees; // Calculate delay and time of animation for progress

        enrichedDonutPart.progress.animationDelayTime = sumProgressAnimationDelayTime;
        enrichedDonutPart.progress.animationTime = maximumAnimationTimePerPart / 100 * enrichedDonutPart.progress.valueInPercent;
        sumProgressAnimationDelayTime += enrichedDonutPart.progress.animationTime; // Calculate delay and time of animation for prognosis

        enrichedDonutPart.prognosis.animationDelayTime = sumPrognosisAnimationDelayTime;
        enrichedDonutPart.prognosis.animationTime = maximumAnimationTimePerPart / 100 * enrichedDonutPart.prognosis.valueInPercent;
        sumPrognosisAnimationDelayTime += enrichedDonutPart.prognosis.animationTime;
        donutData.listOfDonutParts.push(enrichedDonutPart);
      }

      return donutData;
    }
    /**
     * Set data for the donut chart
     * @param parts {array} Array of parts
     * @param progressVolume {number}
     * @param prognosisVolume {number}
     */

  }, {
    key: "setData",
    value: function setData(_ref) {
      var parts = _ref.parts,
          progressVolume = _ref.progressVolume,
          prognosisVolume = _ref.prognosisVolume;
      this.donutData = this.processData(parts);
      this.donutData.progressVolume = progressVolume || "error ";
      this.donutData.prognosisVolume = prognosisVolume || "error ";
      this.init();
    }
    /**
     * Calculate circle outline by radius
     * @param radius {number}
     */

  }, {
    key: "getCircleOutlineLength",
    value: function getCircleOutlineLength(radius) {
      return radius * (Math.PI * 2);
    }
    /**
     * Appends donut chart to the element and initialize it.
     * @param selector {string} Dom element
     */

  }, {
    key: "appendTo",
    value: function appendTo(selector) {
      this.donutChartParentEl = document.querySelector(selector); // Check if element exists

      if (this.donutChartParentEl) {
        // Check if data is set
        if (this.donutData) {
          this.donutChartParentEl.appendChild(this.svgEl); // After rendering and appending svg element adjust font size.

          this.adjustFontSize();
        } else {
          console.error("Donut chart data is missing use .setData() method");
        }
      } else {
        console.error("Can`t find ".concat(selector, " element"));
      }
    }
    /**
     * Set name of the label, progress, prognosis
     *
     * @param labelName {string}
     * @param progressName {string}
     * @param prognosisName {string}
     */

  }, {
    key: "setText",
    value: function setText(_ref2) {
      var labelName = _ref2.labelName,
          progressName = _ref2.progressName,
          prognosisName = _ref2.prognosisName;
      this.options.label.name = labelName || "Not set";
      this.options.legend.progress.name = progressName || "Not set";
      this.options.legend.prognosis.name = prognosisName || "Not set";
    }
  }]);

  return DonutChart;
}();

export { DonutChart as default };