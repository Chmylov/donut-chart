function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * For easy debugging javaScript.
 * Example set debug to true to see debug information.
 */
var Log =
/*#__PURE__*/
function () {
  function Log() {
    _classCallCheck(this, Log);

    this.logLevel = {
      info: false,
      debug: false,
      error: false,
      warning: false
    };
  }

  _createClass(Log, [{
    key: "info",
    value: function info(message) {
      if (this.logLevel.info) {
        console.log(message);
      }
    }
  }, {
    key: "debug",
    value: function debug(message) {
      if (this.logLevel.debug) {
        console.debug(message);
      }
    }
  }, {
    key: "error",
    value: function error(message) {
      if (this.logLevel.error) {
        console.error(message);
      }
    }
  }, {
    key: "warn",
    value: function warn(message) {
      if (this.logLevel.warning) {
        console.warn(message);
      }
    }
  }]);

  return Log;
}();

export { Log as default };