export default class DonutChart {
    constructor() {
        this.options = {
            svgClassName: null,
            partGap: 5,
            donutWidth: 25,
            donutRadius: 100,
            animationTimeInMs: 800,
            label: {
                offset: 65,
                fontSize: 15,
                name: "Not set"
            },
            colors: {
                donutPart: "#e9e9e9",
                prognosisBar: "#9EBCA8",
                progressBar: "#5C8D6D"
            },
            legend: {
                progress: {
                    position: {
                        y: 180,
                        x: 173
                    },
                    name: "Not set"
                },
                prognosis: {
                    position: {
                        y: 225,
                        x: 173
                    },
                    name: "Not set"
                }
            }
        };
        this.namespaceURI = "http://www.w3.org/2000/svg";
    }

    /**
     * Initializes donut part if data is set.
     */
    init() {
        // Generates styles element with keyframes for the donut chart animation and appends it to the header
        this.setDonutChartStyles();

        // Create svg element.
        this.svgEl = this.generateSvgEl();

        // Render donut parts and labels
        for (const part of this.donutData.listOfDonutParts) {
            this.renderDonutPart(part, "part");
            this.renderDonutPart(part, "prognosisPart");
            this.renderDonutPart(part, "progressPart");
            this.renderDonutPartLabel(part);
        }

        // Render gap between the parts
        for (const part of this.donutData.listOfDonutParts) {
            this.renderDonutPartGap(part);
        }

        this.renderDonutChartLegend();

        // Adjust font-size of the text if svg is resized.
        window.addEventListener("resize", () => this.adjustFontSize());
    }

    renderDonutChartLegend() {
        const donutChartLegend = document.createElementNS(this.namespaceURI, "g");
        donutChartLegend.setAttribute("class", "donut-chart__legend");

        const progressLegendCircle = document.createElementNS(this.namespaceURI, "circle");
        progressLegendCircle.setAttribute("cy", this.options.legend.progress.position.y);
        progressLegendCircle.setAttribute("cx", "160");
        progressLegendCircle.setAttribute("r", "7");
        progressLegendCircle.setAttribute("fill", this.options.colors.progressBar);

        const prognosisLegendCircle = document.createElementNS(this.namespaceURI, "circle");
        prognosisLegendCircle.setAttribute("cy", this.options.legend.prognosis.position.y);
        prognosisLegendCircle.setAttribute("cx", "160");
        prognosisLegendCircle.setAttribute("r", "7");
        prognosisLegendCircle.setAttribute("fill", this.options.colors.prognosisBar);

        donutChartLegend.appendChild(
            this.generateTextEl(this.options.legend.progress.name, [
                { name: "y", value: this.options.legend.progress.position.y },
                { name: "x", value: this.options.legend.progress.position.x },
                { name: "class", value: "label" }
            ])
        );

        donutChartLegend.appendChild(
            this.generateTextEl(`${this.donutData.progressVolume}hl`, [
                { name: "y", value: this.options.legend.progress.position.y },
                { name: "x", value: this.options.legend.progress.position.x },
                { name: "class", value: "volume" }
            ])
        );

        donutChartLegend.appendChild(
            this.generateTextEl(this.options.legend.prognosis.name, [
                { name: "y", value: this.options.legend.prognosis.position.y },
                { name: "x", value: this.options.legend.prognosis.position.x },
                { name: "class", value: "label" }
            ])
        );

        donutChartLegend.appendChild(
            this.generateTextEl(`${this.donutData.prognosisVolume}hl`, [
                { name: "y", value: this.options.legend.prognosis.position.y },
                { name: "x", value: this.options.legend.prognosis.position.x },
                { name: "class", value: "volume" }
            ])
        );

        donutChartLegend.appendChild(prognosisLegendCircle);
        donutChartLegend.appendChild(progressLegendCircle);

        this.svgEl.appendChild(donutChartLegend);
    }

    /**
     * Generates styles element with keyframes for the donut chart animation and appends it to the header.
     */
    setDonutChartStyles() {
        let css = ``;

        // Add keyframes for animation
        css += `@keyframes donut-chart {from {stroke-dasharray: 0, ${this.getCircleOutlineLength(
            this.options.donutRadius
        )};}}`;

        css += `svg .label {font-weight: bold}`;
        css += `.upper-label-text {font-weight: bold}`;
        css += `@media only screen and (max-width: 330px) {svg .donut-chart__legend {display: none;}}`;

        // If css is present append it the head
        if (css) {
            const stylesEl = document.createElement("style");
            stylesEl.setAttribute("type", "text/css");
            stylesEl.innerHTML = css;
            document.head.appendChild(stylesEl);
        }
    }

    /**
     * Render part of the donut
     * @param part {Object} Starting point of donut part in percentage
     * @param type  {string} Length of donut part in percentage
     */
    renderDonutPart(part, type) {
        let partLengthInDegrees, color, animation, dashLength, startingPoint;
        const circleOutlineLength = this.getCircleOutlineLength(this.options.donutRadius);

        switch (type) {
            case "part":
                partLengthInDegrees = part.length.valueInDegrees;
                color = this.options.colors.donutPart;
                break;
            case "progressPart":
                partLengthInDegrees = part.progress.valueInDegrees;
                color = this.options.colors.progressBar;
                animation = {
                    time: part.progress.animationTime,
                    delay: part.progress.animationDelayTime,
                    timingFunction: "linear"
                };

                if (part.progress.valueInPercent < 100 && part.progress.valueInPercent !== 0) {
                    if (part.prognosis.valueInPercent !== 100 && part.prognosis.valueInPercent !== 0) {
                        if (part.prognosis.valueInPercent < part.progress.valueInPercent) {
                            animation.time += part.prognosis.animationTime;
                            animation.timingFunction = "ease-out";
                        }
                    } else {
                        if (part.prognosis.valueInPercent < part.progress.valueInPercent) {
                            animation.timingFunction = "ease-out";
                        }
                    }
                }

                break;
            case "prognosisPart":
                partLengthInDegrees = part.prognosis.valueInDegrees;
                color = this.options.colors.prognosisBar;
                animation = {
                    time: part.prognosis.animationTime,
                    delay: part.prognosis.animationDelayTime,
                    timingFunction: "linear"
                };

                if (part.prognosis.valueInPercent < 100 && part.prognosis.valueInPercent !== 0) {
                    if (part.progress.valueInPercent !== 100 && part.progress.valueInPercent !== 0) {
                        if (part.progress.valueInPercent < part.prognosis.valueInPercent) {
                            animation.time += part.prognosis.animationTime;
                            animation.timingFunction = "ease-out";
                        }
                    } else {
                        if (part.progress.valueInPercent < part.prognosis.valueInPercent) {
                            animation.timingFunction = "ease-out";
                        }
                    }
                }
                break;
            default:
                console.error("Type has not been set");
                break;
        }

        // Add gap to the donut
        partLengthInDegrees -= this.options.partGap;

        // Prevent negative partLength value
        if (partLengthInDegrees < 0) {
            partLengthInDegrees = 0;
        }

        // Correct starting position of all circles.
        startingPoint = part.min.valueInDegrees - 90;

        // Correct rotation of the circle if gap is set
        startingPoint += this.options.partGap / 2;

        // Calculate outline of circle for rendering
        dashLength = (circleOutlineLength / 360) * partLengthInDegrees;

        const circleEl = this.generateCircleEl(
            [
                { name: "stroke-dasharray", value: `${dashLength}, ${circleOutlineLength}` },
                { name: "transform", value: `rotate(${startingPoint}, 200, 200)` },
                { name: "stroke", value: color }
            ],
            animation
        );

        this.svgEl.appendChild(circleEl);
    }

    /**
     * Generates gap between the donut part
     *
     * @param part {Object}
     * @returns {SVGCircleElement}
     */
    renderDonutPartGap(part) {
        let startingPoint = part.min.valueInDegrees;

        // Correct starting position of all circles.
        startingPoint -= 90;

        // Calculate degrees, radians and radius
        const degrees = part.min.valueInDegrees;
        const radians = this.convertDegreesToRadians(degrees);
        const radius = this.options.donutRadius - 15;

        // Calculate coordinates of the label
        const x = 200 + radius * Math.sin(radians);
        const y = 200 - radius * Math.cos(radians);

        const rect = this.generateRectangleEl([
            { name: "x", value: x - 1 },
            { name: "y", value: y - this.options.partGap },
            { name: "transform", value: `rotate(${startingPoint} ${x} ${y})` }
        ]);

        this.svgEl.appendChild(rect);
    }

    /**
     * Generates circles that looks like parts
     *
     * @param attributes
     * @param animation
     * @returns {SVGCircleElement}
     */
    generateCircleEl(attributes, animation) {
        const circle = document.createElementNS(this.namespaceURI, "circle");
        circle.setAttribute("cy", "200");
        circle.setAttribute("cx", "200");
        circle.setAttribute("stroke-width", this.options.donutWidth.toString());
        circle.setAttribute("r", this.options.donutRadius.toString());
        circle.setAttribute("fill", "none");

        for (const attribute of attributes) {
            circle.setAttribute(attribute.name, attribute.value);
        }

        if (animation) {
            circle.style.animationTimingFunction = animation.timingFunction;
            circle.style.animationDelay = `${animation.delay}ms`;
            circle.style.animationDuration = `${animation.time}ms`;
            circle.style.animationName = `donut-chart`;
            circle.style.animationFillMode = `backwards`;
        }

        return circle;
    }

    /**
     *
     * Generates gap between donut parts
     * @param attributes {object} Part data from donut data
     * @returns {SVGRectElement}
     */
    generateRectangleEl(attributes) {
        const rect = document.createElementNS(this.namespaceURI, "rect");
        rect.setAttribute("width", "30");
        rect.setAttribute("height", (this.options.partGap * 2).toString());
        rect.setAttribute("fill", "white");
        for (const attribute of attributes) {
            rect.setAttribute(attribute.name, attribute.value);
        }
        return rect;
    }

    /**
     *
     * Generates label next to donut part
     * @param part {object} Part data from donut data
     */
    renderDonutPartLabel(part) {
        // Calculate degrees, radians and radius
        const degrees = part.length.valueInDegrees / 2 + part.min.valueInDegrees;
        const radians = this.convertDegreesToRadians(degrees);
        const radius = this.options.donutRadius + this.options.label.offset;

        // Calculate coordinates of the label
        const x = 200 + radius * Math.sin(radians);
        const y = 200 - radius * Math.cos(radians);

        // Generate labels
        const firstLabelText = `${this.options.label.name} ${part.order}`;

        // Last part should not show max value
        let secondLabelText;

        switch (part.order) {
            case 1:
                secondLabelText = `${part.min.value} - ${part.max.value}hl`;
                break;
            case this.donutData.totalParts:
                secondLabelText = `${part.min.value + 1}hl +`;
                break;
            default:
                secondLabelText = `${part.min.value + 1} - ${part.max.value}hl`;
        }

        // Render the label
        this.svgEl.appendChild(
            this.generateTextEl(firstLabelText, [
                { name: "x", value: x },
                { name: "y", value: y },
                { name: "text-anchor", value: "middle" },
                { name: "class", value: "upper-label-text" }
            ])
        );
        this.svgEl.appendChild(
            this.generateTextEl(secondLabelText, [
                { name: "x", value: x },
                { name: "y", value: y },
                { name: "text-anchor", value: "middle" },
                { name: "class", value: "lower-label-text" }
            ])
        );
    }

    /**
     * Adjust font-size of the text in donut chart if user is resizing the svg image.
     */
    adjustFontSize() {
        let newWidth = this.svgEl.getBoundingClientRect().width;
        const newFontSize = this.options.label.fontSize * (400 / newWidth);
        const textEls = this.svgEl.querySelectorAll("text");

        for (const textEl of textEls) {
            textEl.setAttribute("font-size", newFontSize.toString());
            if (textEl.classList.contains("lower-label-text") || textEl.classList.contains("volume")) {
                textEl.setAttribute("dy", (newFontSize * 1.2).toString());
            }
        }
    }

    /**
     *
     * Generates svg tspan element
     * @param text {string}
     * @param attributes {object[]} List of attributes
     * @returns {SVGTextElement}
     */
    generateTextEl(text, attributes) {
        const textEl = document.createElementNS(this.namespaceURI, "text");

        for (const attribute of attributes) {
            textEl.setAttribute(attribute.name, attribute.value);
        }

        textEl.innerHTML = text;
        return textEl;
    }

    /**
     * Creates svg element where donut part will be rendered.
     */
    generateSvgEl() {
        const svgEl = document.createElementNS(this.namespaceURI, "svg");
        svgEl.setAttribute("width", "100%");
        svgEl.setAttribute("height", "100%");
        svgEl.setAttribute("viewBox", "0 0 400 400");
        if (this.options.svgClassName) svgEl.classList.add(this.options.svgClassName);
        return svgEl;
    }

    /**
     * Converts degrees to radians
     * @param degrees {number} Degrees
     */
    convertDegreesToRadians(degrees) {
        return degrees / (180 / Math.PI);
    }

    /**
     * Enrich data for the donut chart
     * @param listOfDonutParts {array} List of donut parts
     */
    processData(listOfDonutParts) {
        // Create data structure, set total parts and total length of the parts
        const donutData = {
            listOfDonutParts: [],
            totalParts: listOfDonutParts.length,
            totalLengthOfParts: listOfDonutParts[listOfDonutParts.length - 1].max
        };

        // Variables are for calculating animation delay
        let sumProgressAnimationDelayTime = 0;
        let sumPrognosisAnimationDelayTime = 0;

        for (let i = 0; i < listOfDonutParts.length; i++) {
            const donutPart = listOfDonutParts[i];

            let enrichedDonutPart = {};

            if (i !== 0) {
                donutPart.min -= 1;
            }

            enrichedDonutPart.order = donutPart.index + 1;

            enrichedDonutPart.min = {
                value: donutPart.min,
                valueInDegrees: (360 / donutData.totalParts) * i
            };

            enrichedDonutPart.max = {
                value: donutPart.max
            };

            enrichedDonutPart.length = {
                value: donutData.totalLengthOfParts / donutData.totalParts,
                valueInDegrees: 360 / donutData.totalParts
            };

            enrichedDonutPart.prognosis = {
                valueInPercent: donutPart.prognosis,
                valueInDegrees: (enrichedDonutPart.length.valueInDegrees / 100) * donutPart.prognosis,
                animationDelayTime: null,
                animationTime: null
            };
            enrichedDonutPart.progress = {
                valueInPercent: donutPart.progress,
                valueInDegrees: (enrichedDonutPart.length.valueInDegrees / 100) * donutPart.progress,
                animationDelayTime: null,
                animationTime: null
            };

            // Store maximum animation time per part for later use
            const maximumAnimationTimePerPart =
                (this.options.animationTimeInMs / 360) * enrichedDonutPart.length.valueInDegrees;

            // Calculate delay and time of animation for progress
            enrichedDonutPart.progress.animationDelayTime = sumProgressAnimationDelayTime;
            enrichedDonutPart.progress.animationTime =
                (maximumAnimationTimePerPart / 100) * enrichedDonutPart.progress.valueInPercent;
            sumProgressAnimationDelayTime += enrichedDonutPart.progress.animationTime;

            // Calculate delay and time of animation for prognosis
            enrichedDonutPart.prognosis.animationDelayTime = sumPrognosisAnimationDelayTime;
            enrichedDonutPart.prognosis.animationTime =
                (maximumAnimationTimePerPart / 100) * enrichedDonutPart.prognosis.valueInPercent;
            sumPrognosisAnimationDelayTime += enrichedDonutPart.prognosis.animationTime;

            donutData.listOfDonutParts.push(enrichedDonutPart);
        }

        return donutData;
    }

    /**
     * Set data for the donut chart
     * @param parts {array} Array of parts
     * @param progressVolume {number}
     * @param prognosisVolume {number}
     */
    setData({ parts, progressVolume, prognosisVolume }) {
        this.donutData = this.processData(parts);
        this.donutData.progressVolume = progressVolume || 0;
        this.donutData.prognosisVolume = prognosisVolume || 0;
        this.init();
    }

    /**
     * Calculate circle outline by radius
     * @param radius {number}
     */
    getCircleOutlineLength(radius) {
        return radius * (Math.PI * 2);
    }

    /**
     * Appends donut chart to the element and initialize it.
     * @param selector {string} Dom element
     */
    appendTo(selector) {
        this.donutChartParentEl = document.querySelector(selector);

        // Check if element exists
        if (this.donutChartParentEl) {
            // Check if data is set
            if (this.donutData) {
                this.donutChartParentEl.appendChild(this.svgEl);
                // After rendering and appending svg element adjust font size.
                this.adjustFontSize();
            } else {
                console.error(`Donut chart data is missing use .setData() method`);
            }
        } else {
            console.error(`Can\`t find ${selector} element`);
        }
    }

    /**
     * Set name of the label, progress, prognosis
     *
     * @param labelName {string}
     * @param progressName {string}
     * @param prognosisName {string}
     */
    setText({ labelName, progressName, prognosisName }) {
        this.options.label.name = labelName || "Not set";
        this.options.legend.progress.name = progressName || "Not set";
        this.options.legend.prognosis.name = prognosisName || "Not set";
    }
}
