import DonutChart from "./DonutChart.js";

class Main {
    constructor() {
        this.testData1 = [
            {
                index: 0,
                min: 0,
                max: 30,
                progress: 100,
                prognosis: 100
            },
            {
                index: 1,
                min: 31,
                max: 90,
                progress: 100,
                prognosis: 100
            },
            {
                index: 2,
                min: 91,
                max: 199,
                progress: 20,
                prognosis: 50
            },
            {
                index: 3,
                min: 200,
                max: 325,
                progress: 0,
                prognosis: 0
            }
        ];
        this.testData = [
            {
                index: 1,
                min: 0,
                max: 45,
                progress: 0,
                prognosis: 0
            },
            {
                index: 2,
                min: 45,
                max: 90,
                progress: 0,
                prognosis: 0
            },
            {
                index: 3,
                min: 90,
                max: 135,
                progress: 0,
                prognosis: 0
            },
            {
                index: 4,
                min: 135,
                max: 180,
                progress: 0,
                prognosis: 0
            },
            {
                index: 5,
                min: 180,
                max: 225,
                progress: 0,
                prognosis: 0
            },
            {
                index: 6,
                min: 225,
                max: 270,
                progress: 0,
                prognosis: 0
            },
            {
                index: 7,
                min: 270,
                max: 315,
                progress: 0,
                prognosis: 0
            },
            {
                index: 8,
                min: 315,
                max: 360,
                progress: 0,
                prognosis: 0
            }
        ];
        this.testData2 = [
            {
                index: 0,
                min: 0,
                max: 90,
                progress: 100,
                prognosis: 100
            },
            {
                index: 1,
                min: 90,
                max: 180,
                progress: 45,
                prognosis: 100
            },
            {
                index: 2,
                min: 180,
                max: 270,
                progress: 0,
                prognosis: 100
            },
            {
                index: 3,
                min: 270,
                max: 360,
                progress: 0,
                prognosis: 100
            },
            {
                index: 4,
                min: 361,
                max: 99999,
                progress: 0,
                prognosis: 23
            }
        ];
        this.testData3 = [
            { progress: 100, prognosis: 100, min: 0, max: 15, reward: 80.0, index: 0 },
            { progress: 0, prognosis: 0, min: 16, max: 99999, reward: 100.0, index: 1 }
        ];
        this.figmaTestData = {
            currentDeterminingVolume: 35,
            prognosisDeterminingVolume: 70,
            scales: [
                {
                    progress: 100,
                    prognosis: 100,
                    min: 0,
                    max: 30,
                    index: 0
                },
                {
                    progress: 20,
                    prognosis: 100,
                    min: 31,
                    max: 60,
                    index: 1
                },
                {
                    progress: 0,
                    prognosis: 50,
                    min: 61,
                    max: 90,
                    index: 2
                },
                {
                    progress: 0,
                    prognosis: 0,
                    min: 91,
                    max: 120,
                    index: 3
                },
                {
                    progress: 0,
                    prognosis: 0,
                    min: 121,
                    max: 99999,
                    index: 4
                }
            ]
        };
        this.donutChart = new DonutChart();
        this.donutChart.setText({ labelName: "Staffel", progressName: "2019*", prognosisName: "Prognosis**" });
        this.donutChart.setData({
            parts: this.figmaTestData.scales,
            progressVolume: this.figmaTestData.currentDeterminingVolume,
            prognosisVolume: this.figmaTestData.prognosisDeterminingVolume
        });
        this.donutChart.appendTo("[data-js-donut-chart]");
    }
}

const app = new Main();
